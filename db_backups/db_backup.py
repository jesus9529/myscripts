#!/usr/bin/python3.5
import os
import time
import dropbox
from dotenv import load_dotenv
from plumbum.cmd import pg_dump, mysqldump, tar, rm, mysql, grep, awk, psql

path = os.path.join(os.getcwd(), '.env')
load_dotenv(path)

MYSQL_USER = os.environ.get('MYSQL_USER')
MYSQL_PASS = os.environ.get('MYSQL_PASS')
DBX_TOKEN = os.environ.get('DBX_TOKEN')

print('Getting mysql databases')
mysql_dbs = (mysql['-u', MYSQL_USER, '-p' + MYSQL_PASS, '-e', 'SHOW DATABASES;'])()
print('Converting to array')
mysql_dbs = mysql_dbs.split()
print(mysql_dbs)
ignored_mysql = ['Database', 'information_schema', 'mysql', 'performance_schema', 'phpmyadmin', 'sys']
print('Cleaning array')
for ignore in ignored_mysql:
	print('To ignore: ' + ignore)
	if ignore in mysql_dbs:
		mysql_dbs.pop(mysql_dbs.index(ignore))
		print('Ignoring: ' + ignore)
print(mysql_dbs)
print('Getting postgre databases')
pg_dbs = psql['-l'] | grep['UTF8'] | awk['{print $1}']
pg_dbs = pg_dbs().split()
print(pg_dbs)
ignored_pgsql = ['postgres', 'template0', 'template1']
print('Cleaning array')
for ignore in ignored_pgsql:
	print('To ignore: ' + ignore)
	if ignore in pg_dbs:
		pg_dbs.pop(pg_dbs.index(ignore))
		print('Ignoring: ' + ignore)
print(pg_dbs)

dbx = dropbox.Dropbox(DBX_TOKEN)

print('Getting actual datetime')
date = time.asctime()
print('Generating directory name')
dirname = date.replace(' ', '_')
dirname = dirname.replace(':', '_')
print('Creating directory')
os.mkdir(dirname)
os.mkdir(dirname + '/postgre')
os.mkdir(dirname + '/mysql')
print('Postgre backup')
for db in pg_dbs:
	print('Backup: ' + db)
	(pg_dump[db] > dirname + '/postgre/' + db +'.sql')()
print('MySql backup')
for db in mysql_dbs:
	print('Backup: ' + db)
	(mysqldump['-u', MYSQL_USER , '-p' + MYSQL_PASS , db] > dirname + '/mysql/' + db + '.sql')()
print('Saving backup path')
backup_path = os.getcwd()+'/'+dirname
print('Compressing data')
(tar['-zcvf', dirname + '.tar.gz', dirname])()
print('Deleting directory')
(rm['-r', dirname])()
print('Uploading compress file to dropbox')
with open(os.getcwd()+'/'+dirname+'.tar.gz', 'rb') as f:
	dbx.files_upload(f.read(), '/db_backups/'+dirname+'.tar.gz', mute = True)
print('Done')